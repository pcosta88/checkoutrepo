﻿
namespace CheckOut.Interfaces
{
    public interface IItems
    {
        IItemModel GetItem(string item);
        IItemModel GetItem(string item, int quantity);
    }
}
