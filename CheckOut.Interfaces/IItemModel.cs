﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckOut.Interfaces
{
    public interface IItemModel
    {
        string Item { get; set; }
        int Price { get; set; }
    }
}
