﻿using System;
using System.Collections.Generic;
using System.Globalization;
using CheckOut.Interfaces;
using CheckOutService.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CheckOut.Tests
{
    [TestClass]
    public class TestingScan
    {
        private readonly ICheckOut _checkOut;

        private readonly List<IItemModel> _items = new List<IItemModel>(){
            new TestItemModel("A",50),
            new TestItemModel("B",30),
            new TestItemModel("C",20),
            new TestItemModel("D",15)
        };

        private readonly List<ISpecialItemModel> _specialItems = new List<ISpecialItemModel>(){
            new SpecialItemTestModel(new TestItemModel("A",130),3),
            new SpecialItemTestModel(new TestItemModel("B",45),2)
        };

        public TestingScan()
        {
            
            _checkOut = new CheckOutService.Services.CheckOutService(new ItemService(_items, _specialItems));
        }

        [TestMethod]
        public void CheckTotalPrice()
        {
            Assert.AreEqual(0,_checkOut.GetTotalPrice());
        }

        [TestMethod]
        public void ScanFailItem()
        {
            Assert.AreEqual("A",_checkOut.Scan("A"));
        }

        [DataTestMethod]
        [DataRow("A",50)]
        [DataRow("B",30)]
        public void ShouldGetTotalScannedPrice(string item, int expectedPrice)
        {
            _checkOut.Scan(item);
            Assert.AreEqual(expectedPrice,_checkOut.GetTotalPrice());
        }

        [TestMethod]
        public void ShouldGetTotalWithSpecialPrice()
        {
            _checkOut.Scan("A");
            _checkOut.Scan("A");
            _checkOut.Scan("A");
            Assert.AreEqual(130,_checkOut.GetTotalPrice());
        }

        [TestMethod]
        public void AnotherScanWithSpecialPrice()
        {
            _checkOut.Scan("B");
            _checkOut.Scan("B");
            Assert.AreEqual(45, _checkOut.GetTotalPrice());
        }

        [TestMethod]
        public void ScanningNotAvailableItem()
        {
            
            Assert.IsNull(_checkOut.Scan("E"));
        }

        [TestMethod]
        public void ScanAll()
        {
            _checkOut.Scan("A");
            _checkOut.Scan("A");
            _checkOut.Scan("A");
            _checkOut.Scan("B");
            _checkOut.Scan("B");
            _checkOut.Scan("C");
            _checkOut.Scan("D");
            Assert.AreEqual(210,_checkOut.GetTotalPrice());
        }
    }
}
