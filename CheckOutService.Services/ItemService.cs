﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CheckOut.Interfaces;


namespace CheckOutService.Services
{
    public class ItemService : IItems
    {
        private readonly List<IItemModel> _items;
        private readonly List<ISpecialItemModel> _specialItems;

        public ItemService(List<IItemModel> items, List<ISpecialItemModel> specialItems)
        {
            _items = items;
            _specialItems = specialItems;
        }

        public IItemModel GetItem(string item)
        {
            var itemPrice = _items.FirstOrDefault(x=>x.Item == item);

            return itemPrice;
        }

        public IItemModel GetItem(string item, int quantity)
        {
            // check if Item if in the special price list with provided quantity

            var itemPrice = (IItemModel)_specialItems.FirstOrDefault(x => x.Item == item && x.Quantity == quantity);

            if (itemPrice == null)
            {
                itemPrice = GetItem(item);
            }
            
            return itemPrice;
        }
    }
}
