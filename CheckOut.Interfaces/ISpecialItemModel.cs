﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckOut.Interfaces
{
    public interface ISpecialItemModel:IItemModel
    {
        int Quantity { get; set; }
    }
}
