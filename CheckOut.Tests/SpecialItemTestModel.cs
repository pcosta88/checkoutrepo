﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CheckOut.Interfaces;

namespace CheckOut.Tests
{
    public class SpecialItemTestModel:TestItemModel, ISpecialItemModel
    {
        public int Quantity { get; set; }

        public SpecialItemTestModel() { }


        public SpecialItemTestModel(IItemModel item, int quantity):base(item.Item,item.Price)
        {
            Quantity = quantity;
        }
    }
}
