﻿using CheckOut.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckOutService.Services
{
    public class CheckOutService:ICheckOut
    {

        private readonly List<IItemModel> _scannedItems;
        private readonly IItems _items;

        public CheckOutService(IItems items)
        {
            _scannedItems = new List<IItemModel>();
            _items = items;
        }

        public string Scan(string item)
        {
            var itemModel = _items.GetItem(item);

            if (itemModel == null)
            {
                return null;
            }

            _scannedItems.Add(itemModel);

            return item;
        }

        public int GetTotalPrice()
        {
            //Group The Scanned items by quantities...

            var itemGrouping = _scannedItems.GroupBy(x=>x.Item);
            int total = 0;
            foreach (var group in itemGrouping)
            {
                total+=_items.GetItem(group.Key,group.Count()).Price;
            }

            return total;
        }
    }
}
