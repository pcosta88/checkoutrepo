﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CheckOut.Interfaces;

namespace CheckOut.Tests
{
    public class TestItemModel:IItemModel
    {
        public string Item { get; set; }
        public int Price { get; set; }

        public TestItemModel() { }

        public TestItemModel(string item, int price)
        {
            Item = item;
            Price = price;
        }
    }
}
